from rest_framework import serializers

from .models import User, Request, Reply, PositioningInfo

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name']


class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = ['id', 'type', 'image']


class PositioningInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositioningInfo
        fields = ['id', 'latitude', 'longitude', 'temperature', 'temperature_max', 'temperature_min', 'humidity', 'weather', 'city', 'state']


class ReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = Reply
        fields = ['id', 'message', 'calculatedValues', 'climateInfo', 'cultureInfo']