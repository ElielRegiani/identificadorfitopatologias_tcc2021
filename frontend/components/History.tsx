import * as React from 'react';
import { Text, View } from './Themed';


export function History(props: any){

    var historyStructure = props.historyData.map((history: { diseaseName: any; date: any; diseaseInformation: any; }) => {
        return (
        <View style={{ padding: 10}} key={history.diseaseName} >
            <View style={{ borderBottomColor: '#E5E5E5', borderBottomWidth: 1, width: 380,}}/>
            <View style={{flexDirection: 'row', 
                            alignItems: 'center',
                            justifyContent: 'space-between'}}>
                <Text style={{fontFamily: 'philosopher', color: '#002140',fontSize: 20, padding: 20}}>{history.diseaseName}</Text>
                <Text style={{fontFamily: 'philosopher', color: '#002140'}}>{history.date}</Text>
            </View>
                <Text style={{fontFamily: 'philosopher', color: '#002140'}}>{history.diseaseInformation}</Text>
        </View>
        );
    });

    historyStructure.push(
        <View style={{padding: 10}} key = {'Linha_final'}>
                <View style={{ borderBottomColor: '#E5E5E5', borderBottomWidth: 1, width: 380}}/>
        </View>
    )

    return historyStructure;
}