import * as React from 'react';
import { StyleSheet, Image } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { Text, View } from '../components/Themed';

export default function Diagnostico({ route }) {
  const {imageUri} = route.params;
  return (
    <View style={styles.mainContainer}>
      <View style={styles.navBar}>
        <Text style={{alignItems: 'center', margin: 20, fontSize: 30, fontFamily: 'philosopher',
         color: '#002140'}}>Ferrugem</Text>
        <Image style={{ width: 175, height: 175, borderRadius: 50, margin: 20}} source=
        {{ uri: imageUri }}/>
      </View>
      <View style={styles.body}>
        <View style={styles.visaoGeral}>
          <Text style={{marginBottom:'5%', fontSize: 18, fontFamily: 'philosopher', color: '#002140'}}>Visão geral</Text>
          <Text style={{fontSize: 15, fontFamily: 'philosopher', color: '#002140'}}>
            <FontAwesome name="flask" size={20} color="#000"/> Tratamento</Text>
          <Text style={{margin: 20, fontSize: 12, fontFamily: 'philosopher', color: '#002140'}}>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book.
          </Text>
          <Text style={{fontSize: 15, fontFamily: 'philosopher', color: '#002140'}}
          ><FontAwesome name="heart" size={20} color="#000"/> Prevenção</Text>
          <Text style={{margin: 20, fontSize: 12, fontFamily: 'philosopher', color: '#002140'}}>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book.
          </Text>
          <Text style={{fontSize: 18, fontFamily: 'philosopher', color: '#002140'}}>Informações adicionais</Text>
        <Text style={{margin: 20, fontSize: 12, fontFamily: 'philosopher', color: '#002140'}}>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and scrambled it to make a type 
          specimen book.
        </Text>
        </View>
        
      </View>
</View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  navBar: {
      height: '32%',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
  },
  body: {
    flex: 3,
    display: 'flex',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  visaoGeral:{
    margin: 20,
  }
});
