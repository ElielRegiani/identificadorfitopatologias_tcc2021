import * as React from 'react';
import { StyleSheet } from 'react-native';

import { Text, View } from '../components/Themed';
import { History } from '../components/History';
import * as Requester from "../requester/data";

export default function HistoryScreen() {
  const history: { diseaseName: any; date: any; diseaseInformation: any; }[] = Requester.historyData();
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Histórico</Text>
      <History historyData={history} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginBottom: 30,
    fontSize: 30,
    fontFamily: 'philosopher',
    color: '#002140',
    height: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
